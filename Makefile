# Enable PRINTF (with specific runtime on Ledger)
DEBUG = 0
# Enable PRINTF_STACK_SIZE (with modified Bolos)
DEBUG_MEM = 0
# Exception contains line number in parser instead of parse error
DEBUG_PARSER = 0

ifeq ($(TARGET),NANOX)
TARGET_ID=0x33000004
TARGET_NAME=TARGET_NANOX
TARGET_SHORT=nano-x
else
TARGET_SHORT=nano-s
endif

# You can use APP=Tezos to build a Tezos-compatible app

ifeq ($(APP),)
APPNAME = "Dune"
KIND=dune
DEFINES+= DUNE_APP
else
APPNAME = "Tezos"
KIND=tezos
DEFINES+= TEZOS_APP
endif
APP_LOAD_PARAMS= --appFlags 0 --curve ed25519 --curve secp256k1 --curve prime256r1 --path "44'/1729'" $(COMMON_LOAD_PARAMS)

GIT_DESCRIBE ?= $(shell git describe --tags --abbrev=8 --always --long --dirty 2>/dev/null)

VERSION_TAG ?= $(shell echo "$(GIT_DESCRIBE)" | cut -f1 -d-)
APPVERSION_M=0
APPVERSION_N=6
APPVERSION_P=0
APPVERSION_D=20191206
APPVERSION=$(APPVERSION_M).$(APPVERSION_N).$(APPVERSION_P)-$(APPVERSION_D)

# Only warn about version tags if specified/inferred
ifeq ($(VERSION_TAG),)
  $(warning VERSION_TAG not checked)
else
  ifneq (v$(APPVERSION), $(VERSION_TAG))
    $(warning Version-Tag Mismatch: v$(APPVERSION) version and $(VERSION_TAG) tag)
  endif
endif

COMMIT ?= $(shell echo "$(GIT_DESCRIBE)" | awk -F'-g' '{print $$2}' | sed 's/-dirty/*/')
ifeq ($(COMMIT),)
  $(warning COMMIT not specified and could not be determined with git from "$(GIT_DESCRIBE)")
else
  $(info COMMIT=$(COMMIT))
endif

ICONNAME=icons/${TARGET_SHORT}-${KIND}.gif

ROOT_DIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

ifeq ($(BOLOS_SDK),)
$(info Updating Nano S SDK)
$(shell git submodule update --init nanos-secure-sdk)
BOLOS_SDK = $(ROOT_DIR)nanos-secure-sdk
endif
$(info BOLOS_SDK=$(BOLOS_SDK))

ifeq ($(BOLOS_ENV),)
ifneq ($(wildcard $(ROOT_DIR)bolos_env/.*),)
BOLOS_ENV := $(ROOT_DIR)bolos_env
endif
endif

include $(BOLOS_SDK)/Makefile.defines

################
# Default rule #
################
all: show-app default

obj/apdu_version.o: Makefile

.PHONY: show-app
show-app:
	@echo ">>>>> Building $(APP) at commit $(COMMIT)"

show-nano:
	@echo TARGET: ID=${TARGET_ID} NAME=${TARGET_NAME}

watch-log:
	usbtool -v 0x2c97 log

############
# Platform #
############

DEFINES   += OS_IO_SEPROXYHAL
DEFINES   += HAVE_BAGL HAVE_SPRINTF
DEFINES   += HAVE_IO_USB HAVE_L4_USBLIB IO_USB_MAX_ENDPOINTS=6 IO_HID_EP_LENGTH=64 HAVE_USB_APDU
DEFINES   += HAVE_LEGACY_PID
DEFINES   += VERSION=\"$(APPVERSION)\" APPVERSION_M=$(APPVERSION_M)
DEFINES   += COMMIT=\"$(COMMIT)\" APPVERSION_N=$(APPVERSION_N) APPVERSION_P=$(APPVERSION_P)
# DEFINES   += _Static_assert\(...\)=
DEFINES   += APPVERSION_D=\"$(APPVERSION_D)\"

# U2F
# Note: using U2F causes 148 bytes on the stack
DEFINES   += HAVE_U2F HAVE_IO_U2F
DEFINES   += U2F_PROXY_MAGIC=\"XTZ\" # Let's remain compatible
DEFINES   += USB_SEGMENT_SIZE=64
DEFINES   += BLE_SEGMENT_SIZE=32 #max MTU, min 20

DEFINES   += HAVE_BOLOS_APP_STACK_CANARY

DEFINES   += UNUSED\(x\)=\(void\)x

ifeq ($(TARGET_NAME),TARGET_NANOX)
APP_LOAD_PARAMS += --appFlags 0x240 # with BLE support
DEFINES   += IO_SEPROXYHAL_BUFFER_SIZE_B=300
DEFINES   += HAVE_BLE BLE_COMMAND_TIMEOUT_MS=2000
DEFINES   += HAVE_BLE_APDU # basic ledger apdu transport over BLE

DEFINES   += HAVE_GLO096 HAVE_UX_FLOW
DEFINES   += HAVE_BAGL BAGL_WIDTH=128 BAGL_HEIGHT=64
DEFINES   += HAVE_BAGL_ELLIPSIS # long label truncation feature
DEFINES   += HAVE_BAGL_FONT_OPEN_SANS_REGULAR_11PX
DEFINES   += HAVE_BAGL_FONT_OPEN_SANS_EXTRABOLD_11PX
DEFINES   += HAVE_BAGL_FONT_OPEN_SANS_LIGHT_16PX

SDK_SOURCE_PATH  += lib_blewbxx lib_blewbxx_impl
SDK_SOURCE_PATH  += lib_ux
else
DEFINES   += IO_SEPROXYHAL_BUFFER_SIZE_B=128
endif

ifeq ($(DEBUG_MEM),1)
DEFINES   += HAVE_PATCHED_BOLOS
endif

ifeq ($(DEBUG_PARSER),1)
DEFINES   += DEBUG_PARSER
endif

ifneq ($(DEBUG),0)

        DEFINES += DUNE_DEBUG

        ifeq ($(TARGET_NAME),TARGET_NANOX)
                DEFINES   += HAVE_PRINTF PRINTF=mcu_usb_printf
        else
                DEFINES   += HAVE_PRINTF PRINTF=screen_printf
        endif
else
        DEFINES   += PRINTF\(...\)=
endif

.PHONY: distrib

ARCHIVE:=ledger-app-dune-${TARGET_SHORT}-bin-${APPVERSION}${APPSUFFIX}

distrib:
	cat debug/app.map | grep _nvram_data | tr -s ' ' | cut -f2 -d' ' > distrib/nvram_data.txt
	cat debug/app.map | grep _envram_data | tr -s ' ' | cut -f2 -d' ' > distrib/envram_data.txt
	python ${BOLOS_SDK}/icon.py ${ICONNAME} hexbitmaponly > distrib/icon.hex
	cp bin/app.hex distrib/
	echo ${COMMIT} > distrib/VERSION
	cp -dpR distrib ${ARCHIVE}
	tar zcf ${ARCHIVE}.tar.gz ${ARCHIVE}
	rm -rf ${ARCHIVE}

push: distrib
	scp ${ARCHIVE}.tar.gz dune.network:/home/www.dune.network/www/files/ledger-app-dune/${TARGET_SHORT}/
	ssh dune.network "cd /home/www.dune.network/www/files/ledger-app-dune/${TARGET_SHORT}/; ln -sf ${ARCHIVE}.tar.gz latest.tar.gz"

##############
# Compiler #
##############
ifneq ($(BOLOS_ENV),)
$(info BOLOS_ENV=$(BOLOS_ENV))
CLANGPATH := $(BOLOS_ENV)/clang-arm-fropi/bin/
GCCPATH := $(BOLOS_ENV)/gcc-arm-none-eabi-5_3-2016q1/bin/
CFLAGS += -idirafter $(BOLOS_ENV)/gcc-arm-none-eabi-5_3-2016q1/arm-none-eabi/include
else
$(info BOLOS_ENV is not set: falling back to CLANGPATH and GCCPATH)
endif
ifeq ($(CLANGPATH),)
$(info CLANGPATH is not set: clang will be used from PATH)
endif
ifeq ($(GCCPATH),)
$(info GCCPATH is not set: arm-none-eabi-* will be used from PATH)
endif

CC       := $(CLANGPATH)clang

CFLAGS   += -O3 -Os -Wall -Wextra 

AS     := $(GCCPATH)arm-none-eabi-gcc

LD       := $(GCCPATH)arm-none-eabi-gcc
LDFLAGS  += -O3 -Os
LDLIBS   += -lm -lgcc -lc

# import rules to compile glyphs(/pone)
include $(BOLOS_SDK)/Makefile.glyphs

### computed variables
APP_SOURCE_PATH  += src
SDK_SOURCE_PATH  += lib_stusb lib_stusb_impl lib_u2f

load: all
	python -m ledgerblue.loadApp $(APP_LOAD_PARAMS)

delete:
	python -m ledgerblue.deleteApp $(COMMON_DELETE_PARAMS)

# import generic rules from the sdk
include $(BOLOS_SDK)/Makefile.rules

#add dependency on custom makefile filename
dep/%.d: %.c Makefile

listvariants:
	@echo VARIANTS APP tezos_wallet tezos_baking

# Generate delegates from baker list
src/delegates.h: tools/gen-delegates.sh tools/BakersRegistryCoreUnfilteredData.json
	./tools/gen-delegates.sh ./tools/BakersRegistryCoreUnfilteredData.json
dep/to_string.d: src/delegates.h
