
#include "baking_auth.h"

#include "apdu.h"
#include "globals.h"
#include "keys.h"
#include "types.h"
#include "protocol.h"
#include "to_string.h"
#include "ui.h"

#include "os_cx.h"

#include <string.h>

struct block_wire {
  uint8_t magic_byte;
  uint32_t chain_id;
  uint32_t level;
  uint8_t proto;
  // ... beyond this we don't care
} __attribute__((packed));

struct endorsement_wire {
  uint8_t magic_byte;
  uint32_t chain_id;
  uint8_t branch[32];
  uint8_t tag;
  uint32_t level;
} __attribute__((packed));

bool parse_baking_data(parsed_baking_data_t *const out,
                       void const *const data, size_t const length) {
  switch (get_magic_byte(data, length)) {
  case MAGIC_BYTE_BAKING_OP:
    if (length != sizeof(struct endorsement_wire)) return false;
    struct endorsement_wire const *const endorsement = data;
    out->baking_kind = BAKING_KIND_ENDORSEMENT;
    out->chain_id.v =
      READ_UNALIGNED_BIG_ENDIAN(uint32_t, &endorsement->chain_id);
    out->level = READ_UNALIGNED_BIG_ENDIAN(uint32_t, &endorsement->level);
    return true;
  case MAGIC_BYTE_BLOCK:
    if (length < sizeof(struct block_wire)) return false;
    struct block_wire const *const block = data;
    out->baking_kind = BAKING_KIND_BLOCK;
    out->chain_id.v = READ_UNALIGNED_BIG_ENDIAN(uint32_t, &block->chain_id);
    out->level = READ_UNALIGNED_BIG_ENDIAN(level_t, &block->level);
    return true;
  case MAGIC_BYTE_TEST:
    out->baking_kind = BAKING_KIND_TEST;
    out->chain_id.v = mainnet_chain_id.v;
    out->level = 1;
    return true;
  case MAGIC_BYTE_INVALID:
  default:
    return false;
  }
}

