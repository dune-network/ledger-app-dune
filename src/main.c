#include "apdu_baking.h"
#include "apdu_hmac.h"
#include "apdu_pubkey.h"
#include "apdu_setup.h"
#include "apdu_sign.h"
#include "apdu.h"
#include "globals.h"
#include "types.h"
#include "apdu_version.h"


__attribute__((noreturn))
void main_loop(void) {
    volatile size_t rx = io_exchange(CHANNEL_APDU, 0);
    while (true) {
      PRINTF_STACK_SIZE("main_loop");
        BEGIN_TRY {
            TRY {
                // Process APDU of size rx
                if (rx == 0) {
                    // no apdu received, well, reset the session, and reset the
                    // bootloader configuration
                    THROW(EXC_SECURITY);
                }

                if (G_io_apdu_buffer[OFFSET_CLA] != CLA) {
                    THROW(EXC_CLASS);
                }

                // The amount of bytes we get in our APDU must match what the APDU declares
                // its own content length is. All these values are unsigned, so this implies
                // that if rx < OFFSET_CDATA it also throws.
                if (rx != G_io_apdu_buffer[OFFSET_LC] + OFFSET_CDATA) {
                    THROW(EXC_WRONG_LENGTH);
                }

                uint8_t const instruction = G_io_apdu_buffer[OFFSET_INS];
                PRINTF("  instruction=%d\n", instruction);
        size_t tx = 0;
        switch(instruction){
        case INS_VERSION :
          tx = handle_apdu_version(instruction); break;
        case INS_GET_PUBLIC_KEY :
          tx = handle_apdu_get_public_key(instruction); break;
        case INS_PROMPT_PUBLIC_KEY :
          tx = handle_apdu_get_public_key(instruction); break;
        case INS_SIGN :
          tx = handle_apdu_sign(instruction); break;
        case INS_GIT :
          tx = handle_apdu_git(instruction); break;
        case INS_SIGN_WITH_HASH :
          tx = handle_apdu_sign(instruction); break;
        case INS_AUTHORIZE_BAKING :
          tx = handle_apdu_get_public_key(instruction); break;
        case INS_RESET :
          tx = handle_apdu_reset(instruction); break;
        case INS_QUERY_AUTH_KEY :
          tx = handle_apdu_query_auth_key(instruction); break;
        case INS_QUERY_MAIN_HWM :
          tx = handle_apdu_main_hwm(instruction); break;
        case INS_SETUP :
          tx = handle_apdu_setup(instruction); break;
        case INS_QUERY_ALL_HWM :
          tx = handle_apdu_all_hwm(instruction); break;
        case INS_DEAUTHORIZE :
          tx = handle_apdu_deauthorize(instruction); break;
        case INS_QUERY_AUTH_KEY_WITH_CURVE :
          tx = handle_apdu_query_auth_key_with_curve(instruction); break;
        case INS_HMAC :
          tx = handle_apdu_hmac(instruction); break;
        case INS_SIGN_UNSAFE :
          tx = handle_apdu_sign(instruction); break;
        case INS_SET_BAKING_MODE :
          tx = handle_set_baking_mode(instruction); break;
        case INS_SET_BAKING_ALWAYS :
          tx = handle_set_baking_always(instruction); break;
        case INS_SETUP_MORE :
          tx = handle_apdu_setup(instruction); break;
        default: tx = handle_apdu_error (instruction);
        }
 

                
                rx = io_exchange(CHANNEL_APDU, tx);
            }
            CATCH(ASYNC_EXCEPTION) {
                rx = io_exchange(CHANNEL_APDU | IO_ASYNCH_REPLY, 0);
            }
            CATCH(EXCEPTION_IO_RESET) {
                THROW(EXCEPTION_IO_RESET);
            }
            CATCH_OTHER(e) {
                clear_apdu_globals(); // IMPORTANT: Application state must not persist through errors

                uint16_t sw = e;
                switch (sw) {
                default:
                    sw = 0x6800 | (e & 0x7FF);
                    // FALL THROUGH
                case 0x6000 ... 0x6FFF:
                case 0x9000 ... 0x9FFF: {
                        size_t tx = 0;
                        G_io_apdu_buffer[tx++] = sw >> 8;
                        G_io_apdu_buffer[tx++] = sw;
                        rx = io_exchange(CHANNEL_APDU, tx);
                        break;
                    }
                }
            }
            FINALLY {
            }
        }
        END_TRY;
    }
}
