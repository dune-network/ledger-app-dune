#pragma once

#include "apdu.h"

extern size_t handle_apdu_sign(uint8_t const instruction);
