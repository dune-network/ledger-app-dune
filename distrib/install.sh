#!/bin/sh

set -v

python -m ledgerblue.loadApp --appFlags 0 --curve ed25519 --curve secp256k1 --curve prime256r1 --path "44'/1729'" --tlv --targetId 0x31100004 --delete --fileName app.hex --appName "Dune" --appVersion `cat VERSION` --dataSize $((`cat envram_data.txt` - `cat nvram_data.txt`)) --icon `cat icon.hex` --targetVersion=""
