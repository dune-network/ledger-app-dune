Differences with Tezos Ledger app
=================================

This app is based on the Tezos Ledger app, with the following changes:

* the two apps wallet and baking have been merged, with an instruction to
  switch between the two modes.

* it is possible to bake with several keys of the same ledger
  (in `types.h`, `MAX_SETUP_KEYS` is actually set to 3)

* derivation paths are limited to 5 instead of 10
  (in `types.h`, `MAX_BIP32_PATH` is 5)

* there 3 new instructions:
  INS_SET_BAKING_MODE (switch between wallet and baking)
  INS_SET_BAKING_ALWAYS (force to remain in baking mode)
  INS_SETUP_MORE (setup more keys for baking)
  (in `apdu.h`)

* some instructions return more information, in particular when there are
  more keys to bake with.

* transactions and originations with parameters display the values of
  amount, fees, destination, but not the parameters, instead of
  displaying `unrecognized operation`.


